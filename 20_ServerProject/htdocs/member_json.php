<?php
/**
 * json response sample
 *
 */

	// jsonデータ仮
	$array = array(
		array(
			"name" => "ひとりめ" ,
			"age" => 123 ,
			"hobby" => "ゴルフ" ,
		),
		array(
			"name" => "ふたりめ" ,
			"age" => 25 ,
			"hobby" => "walk" ,
		),
		array(
			"name" => "さんにんめ" ,
			"age" => 77 ,
			"hobby" => "山" ,
		)
	);

	// 連想配列($array)をJSONに変換(エンコード)する
	$json = json_encode( $array ) ;

	echo $json;

?>
