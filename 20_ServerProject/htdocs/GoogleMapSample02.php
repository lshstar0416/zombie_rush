<?php
/**
 * Goole API Sample
 *
 */
?>
<!DOCTYPE>
<html lang="ja">
<head>
	<meta charset="utf-8" />
	<title>Google Maps Sample</title>

	<!-- for Smart Phone Viewport Set -->
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

	<!-- for Google Maps API V3 -->
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&key=AIzaSyBNttv0Nd8sHR_pVSSYXvOC16B2h8P7rxg"></script>
	<!-- for User Setting -->
	<script type="text/javascript">
		function initialize() {
			/* set locate */
			var ltMapCenter=new google.maps.LatLng(35.6855695,139.740338);
			var ltMyOffice=new google.maps.LatLng(35.6852968,139.7404128);	/*	office	*/
			/* set map options */
			var myOptionsA = {
				zoom: 17,										/* zoom level */
				center: ltMapCenter,							/* center of map */
				mapTypeId: google.maps.MapTypeId.ROADMAP		/* map type */
			};
			var myOptionsB = {
				zoom: 15,										/* zoom level */
				center: ltMapCenter,							/* center of map */
				mapTypeId: google.maps.MapTypeId.SATELLITE		/* map type */
			};
			var myOptionsC = {
				zoom: 10,										/* zoom level */
				center: ltMapCenter,							/* center of map */
				mapTypeId: google.maps.MapTypeId.HYBRID			/* map type */
			};
			var myOptionsD = {
				zoom: 5,										/* zoom level */
				center: ltMapCenter,							/* center of map */
				mapTypeId: google.maps.MapTypeId.TERRAIN		/* map type */
			};
			/* make map datas */
			var mapMapA	=new google.maps.Map(document.getElementById("CvMapA"), myOptionsA);
			var mapMapB	=new google.maps.Map(document.getElementById("CvMapB"), myOptionsB);
			var mapMapC	=new google.maps.Map(document.getElementById("CvMapC"), myOptionsC);
			var mapMapD	=new google.maps.Map(document.getElementById("CvMapD"), myOptionsD);
			/* make infomation window */
			var infowindow = new google.maps.InfoWindow({
				content: 'クリーク･アンド･リバー社<br/><a href="http://www.cri.co.jp/">LINK</a>',
			});
			/* make marker datas */
			var marker = new google.maps.Marker({
				position: ltMyOffice,
				map: mapMapA, 
				title: "My Office!"
			});
			/* set lisner event */
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(mapMapA,marker);
			});

		}
	</script>
	<style type="text/css">
		html, body { margin:0; padding:0; }
		#CvMapA { width:500; height:500; }
		#CvMapB { width:500; height:500; }
		#CvMapC { width:500; height:500; }
		#CvMapD { width:500; height:500; }
	</style>

</head>
<body onload="initialize()">
	<h1>Google Maps sample 02</h1>
	<h2>Maps View Any Type</h2>
	<!-- map view -->
	<table>
		<tr><td><div id="CvMapA"></div></td><td><div id="CvMapB"></div></td></tr>
		<tr><td>ROADMAP</td><td>SATELLITE</td></tr>
		<tr><td><div id="CvMapC"></div></td><td><div id="CvMapD"></div></td></tr>
		<tr><td>HYBRID</td><td>TERRAIN</td></tr>
	</table>
</body>
</html>


