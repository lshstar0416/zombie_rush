<?php
/**
 * Goole API Sample
 *
 */
?>
<!DOCTYPE>
<html lang="ja">
<head>
	<meta charset="utf-8" />
	<title>Google Maps Sample</title>

	<!-- for Smart Phone Viewport Set -->
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

	<!-- for Google Maps API V3 -->
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&key=AIzaSyBNttv0Nd8sHR_pVSSYXvOC16B2h8P7rxg"></script>
	<!-- for User Setting -->
	<script type="text/javascript">
		var mapMapA;
		var elev;
		
		function initialize() {
			/* set locate */
			var ltMapCenter=new google.maps.LatLng(35.6855695,139.740338);
			/* set map options */
			var myOptionsA = {
				zoom: 17,										/* zoom level */
				center: ltMapCenter,							/* center of map */
				mapTypeId: google.maps.MapTypeId.ROADMAP		/* map type */
			};
			/* make map datas */
			mapMapA	=new google.maps.Map(document.getElementById("CvMapA"), myOptionsA);
			/* set lisner event */
			google.maps.event.addListener(mapMapA, 'click', checkMapPos);
			// 高度リクエストを送信するElevationServiceの作成
			elev = new google.maps.ElevationService();

		}

		function checkMapPos(event) {
			var clickedLocation = event.latLng;

			document.getElementById("id_map_lat").innerHTML = clickedLocation.lat();
			document.getElementById("id_map_lng").innerHTML = clickedLocation.lng();

			mapMapA.panTo( new google.maps.LatLng( clickedLocation.lat(), clickedLocation.lng() ) );
		}

	</script>
	<style type="text/css">
		html, body { margin:0; padding:0; }
		#CvMapA { width:500; height:500; }
	</style>

</head>
<body onload="initialize()">
	<h1>Google Maps Sample 05</h1>
	<ul>
	<li>Touch to Center.</li>
	</ul>

	<!-- map view -->
	<div id="CvMapA"></div>

	<table>
		<tr><td>LAT</td><td><p id="id_map_lat"></p></td></tr>
		<tr><td>LNG</td><td><p id="id_map_lng"></p></td></tr>
	</table>

</body>
</html>
