<?php
/**
 * Post API Sample
 *
 */
?>
<!DOCTYPE>
<html lang="ja">
<head>
	<meta charset="utf-8" />
	<title>Post API Sample</title>

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

	<script type="text/javascript">
		function searchPostAddress() {
		    $.getJSON('http://zipcloud.ibsnet.co.jp/api/search?callback=?',
		      {
		        zipcode: $('#zip').val()
		      }
		    )
		    // 結果を取得したら…
		    .done(function(data) {
		      // 中身が空でなければ、その値を［住所］欄に反映
		      if (data.results) {
		        var result = data.results[0];
		        $('#address').val(result.address1 + result.address2 + result.address3);
		      // 中身が空の場合は、エラーメッセージを反映
		      } else {
		        $('#address').val('該当する住所が存在しません。');
		      }
		    });
		  };
	</script>

</head>
<body>
	<h1>Post API Sample 01</h1>
	<ul>
	<li>Search address.</li>
	</ul>

<form>
	<div>
		<label for="zip">郵便番号：</label><br />
		<input id="zip" type="text" size="10" value="1000001" />
		<input type="button" id="close" value="検索" onclick="searchPostAddress()" />
	</div>
	<div>
		<label for="address">住所：</label><br />
		<input id="address" type="text" size="35" />
	</div>
</form>

・郵便番号検索 - 日本郵便 <br />
http://www.post.japanpost.jp/zipcode/index.html <br />

</body>
</html>