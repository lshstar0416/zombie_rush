<?php
/**
 * json response sample
 *
 */

	// jsonデータ仮
	$array = array(
		array(
			"name" => "ServerData01" ,
			"age" => 123 ,
			"hobby" => "gateball"
		),
		array(
			"name" => "ServerData02" ,
			"age" => 789 ,
			"hobby" => "walk"
		),
	);

	// 連想配列($array)をJSONに変換(エンコード)する
	$json = json_encode( $array ) ;

	echo $json;

?>
