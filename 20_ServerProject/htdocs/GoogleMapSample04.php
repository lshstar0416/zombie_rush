<?php
/**
 * Goole API Sample
 *
 */
?>
<!DOCTYPE>
<html lang="ja">
<head>
	<meta charset="utf-8" />
	<title>Google Maps Sample</title>

	<!-- for Smart Phone Viewport Set -->
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

	<!-- for Google Maps API V3 -->
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&key=AIzaSyBNttv0Nd8sHR_pVSSYXvOC16B2h8P7rxg"></script>
	<!-- for User Setting -->
	<script type="text/javascript">
		var mapMapA;
		var infowindow;
		var geocoder;
		var elevsrv;
		
		function initialize() {
			/* set locate */
			var ltMapCenter=new google.maps.LatLng(35.6855695,139.740338);
			var ltMyOffice=new google.maps.LatLng(35.6852968,139.7404128);	/*	office	*/
			/* set map options */
			var myOptionsA = {
				zoom: 17,										/* zoom level */
				center: ltMapCenter,							/* center of map */
				mapTypeId: google.maps.MapTypeId.ROADMAP		/* map type */
			};
			/* make map datas */
			mapMapA	=new google.maps.Map(document.getElementById("CvMapA"), myOptionsA);
			/* make infomation window */
			infowindow = new google.maps.InfoWindow({
				content: 'クリーク･アンド･リバー社<br/><a href="http://www.cri.co.jp/">LINK</a>',
				position: ltMyOffice
			});
			/* make marker datas */
			var marker = new google.maps.Marker({
				position: ltMyOffice,
				map: mapMapA, 
				title: "My Office!"
			});
			/* set lisner event */
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(mapMapA,marker);
			});
			google.maps.event.addListener(mapMapA, 'maptypeid_changed', checkMapType);
			google.maps.event.addListener(mapMapA, 'click', checkMapPos);

			// 高度リクエストを送信するElevationServiceの作成
			elevsrv	= new google.maps.ElevationService();

			/* make geocoder */
			geocoder	= new google.maps.Geocoder();

			/* after init function */
			checkMapType();
			document.getElementById("id_markerinfomation").innerHTML = "MakerInfomation:OFF";
		}

		function checkMapType() {
			var maptypeid = mapMapA.getMapTypeId();
			var str = "MapType:";
			if (maptypeid == google.maps.MapTypeId.HYBRID){
				str += "HYBRID";
			}else if (maptypeid == google.maps.MapTypeId.ROADMAP){
				str += "ROADMAP";
			}else if (maptypeid == google.maps.MapTypeId.SATELLITE){
				str += "SATELLITE";
			}else if (maptypeid == google.maps.MapTypeId.TERRAIN){
				str += "TERRAIN";
			}
			document.getElementById("id_maptype").innerHTML = str;
		}
		function checkMapPos(event) {
			var clickedLocation = event.latLng;

			document.getElementById("id_map_lat").innerHTML = clickedLocation.lat();
			document.getElementById("id_map_lng").innerHTML = clickedLocation.lng();

			/* try,get elevation */
			var locationlist = [];
			locationlist.push(clickedLocation);
			var positionalRequest = {
				'locations': locationlist
			}
			elevsrv.getElevationForLocations(positionalRequest, elevResultCallback);
		}

		function elevResultCallback(result, status) {
			if (status != google.maps.ElevationStatus.OK) {
				alert(status);
				return;
			}
			document.getElementById("id_map_elv").innerHTML = result[0].elevation.toString() + 'm';
		}

		var iChgMapMarker=0;
		function chgMapMarker() {
			var str = "MakerInfomation:";
			iChgMapMarker^=1;
			if(iChgMapMarker==0){
				infowindow.close();
				str += "OFF";
			}else{
				infowindow.open(mapMapA);
				str += "ON";
			}
			document.getElementById("id_markerinfomation").innerHTML = str;
		}

		var iChgMapType=0;
		function chgMapType() {
			iChgMapType++;
			if(iChgMapType>3){	iChgMapType=0;	}
			switch(iChgMapType){
				case 0:
				mapMapA.setMapTypeId(google.maps.MapTypeId.ROADMAP);
				break;
				case 1:
				mapMapA.setMapTypeId(google.maps.MapTypeId.SATELLITE);
				break;
				case 2:
				mapMapA.setMapTypeId(google.maps.MapTypeId.HYBRID);
				break;
				case 3:
				mapMapA.setMapTypeId(google.maps.MapTypeId.TERRAIN);
				break;
			}
		}


		function buttonpress() {
			// GeocoderRequest
			var req = {
				address: document.getElementById("id_input").value,
			};
			geocoder.geocode(req, geoResultCallback);
		}
		function geoResultCallback(result, status) {
			if (status != google.maps.GeocoderStatus.OK) {
				alert(status);
				return;
			}
			var latlng = result[0].geometry.location;

			/* update map center */
			mapMapA.setCenter(latlng);

			var marker = new google.maps.Marker({position:latlng, map:mapMapA, title:latlng.toString(), draggable:true});
			google.maps.event.addListener(marker, 'dragend', function(event){
				marker.setTitle(event.latLng.toString());
			});
			document.getElementById("id_latlngtext").innerHTML = document.getElementById("id_input").value + " : " + latlng.toString();
		}

	</script>
	<style type="text/css">
		html, body { margin:0; padding:0; }
		#CvMapA { width:500; height:500; }
	</style>

</head>
<body onload="initialize()">
	<h1>Google Maps Sample 04</h1>
	<ul>
	<li>Maker Type Change.</li>
	<li>Map Type Change.</li>
	<li>Get Geo Infomation by TouchPoint</li>
	<li>Get Geolocation</li>
	</ul>

	<!-- map view -->
	<div id="CvMapA"></div>

	<form>
		<p>
		<input type="button" id="close" value="MapMarker" onclick="chgMapMarker()" />
		<input type="button" id="close" value="MapType" onclick="chgMapType()" />
		</p>
	</form>
	<p id="id_markerinfomation"></p>
	<p id="id_maptype"></p>
	<table>
		<tr><td>LAT</td><td><p id="id_map_lat"></p></td></tr>
		<tr><td>LNG</td><td><p id="id_map_lng"></p></td></tr>
		<tr><td>ELV</td><td><p id="id_map_elv"></p></td></tr>
	</table>

	<div>Address:
		<input id="id_input" onsubmit="buttonpress()" />
		<input type="button" onclick="buttonpress()" value="get geolocation" />
	</div>
	<div id="id_latlngtext"></div>


</body>
</html>
