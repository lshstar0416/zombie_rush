<?php
/**
 * Weather API Sample
 *
 */
?>
<!DOCTYPE>
<html lang="ja">
<head>
	<meta charset="utf-8" />
	<title>Weather API Sample</title>

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="jquery.xdomainajax.js"></script> <!-- 追加 -->

	<script type="text/javascript">
		function searchWeather() {
			$.getJSON(
				'http://weather.livedoor.com/forecast/webservice/json/v1?',
				{
					city: $('#city').val()
				}
			).done(
				function(data) {
					if (data) {
						$('#location').val(data.location.area +':'+ data.location.prefecture +':'+ data.location.city);
						$('#title').val(data.title);
						$('#result').val(data.forecasts[0].image.title);
					} else {
						$('#location').val('該当する住所が存在しません。');
					}
				}
			);
		  };
	</script>

</head>
<body>
	<h1>Weather API Sample 02</h1>
	<ul>
	<li>Check Weather.</li>
	</ul>

<form>
	<div>
		<label for="city">地域別に定義されたID番号(130010:東京)：</label><br />
		<input id="city" type="text" size="10" value="130010"/>
		<input type="button" id="close" value="検索" onclick="searchWeather()" />
	</div>
	<br />
	<div>
		地域
		<input id="location" type="text" size="35" />
	</div>
	<div>
		タイトル
		<input id="title" type="text" size="35" />
	</div>
	<div>
		結果
		<input id="result" type="text" size="35" />
	</div>
</form>

・お天気Webサービス仕様 - Weather Hacks - livedoor 天気情報 <br />
http://weather.livedoor.com/weather_hacks/webservice <br />

</body>
</html>
