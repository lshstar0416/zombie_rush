<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gameranking[]|\Cake\Collection\CollectionInterface $gameranking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Gameranking'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="gameranking index large-9 medium-8 columns content">
    <h3><?= __('Gameranking') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Score') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gameranking as $gameranking): ?>
            <tr>
                <td><?= $this->Number->format($gameranking->Id) ?></td>
                <td><?= h($gameranking->Name) ?></td>
                <td><?= $this->Number->format($gameranking->Score) ?></td>
                <td><?= h($gameranking->Date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $gameranking->Id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gameranking->Id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gameranking->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $gameranking->Id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
