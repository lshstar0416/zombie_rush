<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gameranking $gameranking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Gameranking'), ['action' => 'edit', $gameranking->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Gameranking'), ['action' => 'delete', $gameranking->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $gameranking->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Gameranking'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gameranking'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="gameranking view large-9 medium-8 columns content">
    <h3><?= h($gameranking->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($gameranking->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($gameranking->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($gameranking->Score) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($gameranking->Date) ?></td>
        </tr>
    </table>
</div>
