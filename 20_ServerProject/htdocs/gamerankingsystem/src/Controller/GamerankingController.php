<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Gameranking Controller
 *
 * @property \App\Model\Table\GamerankingTable $Gameranking
 *
 * @method \App\Model\Entity\Gameranking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GamerankingController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $gameranking = $this->paginate($this->Gameranking);

        $this->set(compact('gameranking'));
    }

    /**
     * View method
     *
     * @param string|null $id Gameranking id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gameranking = $this->Gameranking->get($id, [
            'contain' => []
        ]);

        $this->set('gameranking', $gameranking);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gameranking = $this->Gameranking->newEntity();
        if ($this->request->is('post')) {
            $gameranking = $this->Gameranking->patchEntity($gameranking, $this->request->getData());
            if ($this->Gameranking->save($gameranking)) {
                $this->Flash->success(__('The gameranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gameranking could not be saved. Please, try again.'));
        }
        $this->set(compact('gameranking'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Gameranking id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gameranking = $this->Gameranking->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gameranking = $this->Gameranking->patchEntity($gameranking, $this->request->getData());
            if ($this->Gameranking->save($gameranking)) {
                $this->Flash->success(__('The gameranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gameranking could not be saved. Please, try again.'));
        }
        $this->set(compact('gameranking'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Gameranking id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gameranking = $this->Gameranking->get($id);
        if ($this->Gameranking->delete($gameranking)) {
            $this->Flash->success(__('The gameranking has been deleted.'));
        } else {
            $this->Flash->error(__('The gameranking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getMessages()
    {
        error_log("getMessages()");

        $this->autoRender = false;

        $query = $this->Gameranking->find('all');

        $json_array = json_encode($query);

        echo $json_array;
    }
}
