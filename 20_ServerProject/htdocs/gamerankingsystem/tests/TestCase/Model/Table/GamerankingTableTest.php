<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GamerankingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GamerankingTable Test Case
 */
class GamerankingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GamerankingTable
     */
    public $Gameranking;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gameranking'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Gameranking') ? [] : ['className' => GamerankingTable::class];
        $this->Gameranking = TableRegistry::getTableLocator()->get('Gameranking', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Gameranking);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
