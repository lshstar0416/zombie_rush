-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- 생성 시간: 20-03-09 09:32
-- 서버 버전: 10.1.37-MariaDB
-- PHP 버전: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 데이터베이스: `techtest`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `gameranking`
--

CREATE TABLE `gameranking` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Score` varchar(30) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 테이블의 덤프 데이터 `gameranking`
--

INSERT INTO `gameranking` (`Id`, `Name`, `Score`, `Date`) VALUES
(1, 'IZZY', '37200', '2020-02-12 11:36:12'),
(2, 'Lee', '23900', '2020-03-08 12:17:31');

--
-- 덤프된 테이블의 인덱스
--

--
-- 테이블의 인덱스 `gameranking`
--
ALTER TABLE `gameranking`
  ADD PRIMARY KEY (`Id`);

--
-- 덤프된 테이블의 AUTO_INCREMENT
--

--
-- 테이블의 AUTO_INCREMENT `gameranking`
--
ALTER TABLE `gameranking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
