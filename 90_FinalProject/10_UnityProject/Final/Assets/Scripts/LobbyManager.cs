﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    private string gameVersion = "1.0.0";
    public Text connetionInfoText;
    public Button joinButton;

    void Start()
    {
        PhotonNetwork.GameVersion = gameVersion;
        PhotonNetwork.ConnectUsingSettings();

        joinButton.interactable = false;
        connetionInfoText.text = "Connecting......";
    }

    public override void OnConnectedToMaster()
    {
        joinButton.interactable = true;
        connetionInfoText.text = "Status: Online || Connection Success!";
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        joinButton.interactable = false;
        connetionInfoText.text = "Status: Offline || Connection Failed!";
        PhotonNetwork.ConnectUsingSettings();
    }

    public void Connect()
    {
        joinButton.interactable = false;

        if (PhotonNetwork.IsConnected)
        {
            connetionInfoText.text = "Status: Online || Join a Room.....";
            PhotonNetwork.JoinRandomRoom();
        }

        else
        {
            connetionInfoText.text = "Status: Offline || Retrying Connection.....";
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        connetionInfoText.text = "No Empty Room! New Room Create.....";
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = 3 });
    }

    public override void OnJoinedRoom()
    {
        connetionInfoText.text = "Join Complete";
        PhotonNetwork.LoadLevel("Map_v1");
    }
}
