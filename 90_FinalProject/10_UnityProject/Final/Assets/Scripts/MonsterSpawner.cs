﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    public Monster monsterPrefab;
    public Transform[] spawnPoints;

    public Color strongMonsterColor = Color.red;
    private List<Monster> monsters = new List<Monster>();

    public float maxDamage = 30f;
    public float minDamage = 10f;
    public float maxHealth = 120f;
    public float minHealth = 60f;
    public float maxSpeed = 3f;
    public float minSpeed = 1f;
    public int wave;
    private bool isWaveClear = false;
    private bool isDelay = false;
    private bool isHelp = false;

    private void Update()
    {
        if (GameManager.instance != null && GameManager.instance.isGameover)
        {
            return;
        }

        if (monsters.Count <= 0)
        {
            NextWave();
        }

        UIUpdate();
    }

    private void UIUpdate()
    {
        UIManager.instance.WaveTextUpdate(wave, monsters.Count);
    }

    private void NextWave()
    {
        wave++;

        isWaveClear = true;
        UIManager.instance.ActiveWaveUI(isWaveClear);

        if (wave == 1)
        {
            isHelp = true;
            UIManager.instance.ActiveHelpUI(isWaveClear);
        }

        Invoke("Wait2Sec", 2f);
        Invoke("Wait5Sec", 5f);

        int spawnCount = Mathf.RoundToInt(wave * 3f);

        for (int i = 0; i < spawnCount; i++)
        {
            if (isDelay == false)
            {
                float enemyIntensity = Random.Range(0f, 6f);
                MonsterCreate(enemyIntensity);
            }
        }
    }

    void Wait2Sec()
    {
        isWaveClear = false;
        UIManager.instance.ActiveWaveUI(isWaveClear);
    }

    void Wait5Sec()
    {
        isHelp = false;
        UIManager.instance.ActiveHelpUI(isHelp);
    }

    private void MonsterCreate(float intensity)
    {
        float health = Mathf.Lerp(minHealth, maxHealth, intensity);
        float damage = Mathf.Lerp(minDamage, maxDamage, intensity);
        float speed = Mathf.Lerp(minSpeed, maxSpeed, intensity);
        Color skin = Color.Lerp(Color.green, strongMonsterColor, intensity);
        Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];

        Monster monster = Instantiate(monsterPrefab, spawnPoint.position, spawnPoint.rotation);

        monster.Setup(health, damage, speed, skin);
        monsters.Add(monster);

        monster.onDeath += () => monsters.Remove(monster);
        monster.onDeath += () => Destroy(monster.gameObject, 7f);
        monster.onDeath += () => GameManager.instance.AddScore(100);
    }
}
