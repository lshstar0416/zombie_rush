﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MiniJSON;
using System;
using System.IO;
using System.Text;
using UnityEngine.Networking;

public class ApiManager : MonoBehaviour
{
    [SerializeField] private Text _informationField = default;
    [SerializeField] private Text _rankingField = default;

    private List<RankingData> rankingList = null;

    public void OnClickShowRankingList()
    {
        string sStrOutput = "";
        
        if (null == rankingList)
        {
            sStrOutput = "no list !";
        }

        else
        {
            foreach (RankingData rankingOne in rankingList)
            {
                sStrOutput += $"◆Name:{rankingOne.Name}\n Score:{rankingOne.Score}\n Date:{rankingOne.Date} \n\n";
            }
        }

        _rankingField.text = sStrOutput;
    }

    public void OnClickGetJsonFromWebRequest()
    {
        _informationField.text = "wait...";
        GetJsonFromWebRequest();
    }

    private void GetJsonFromWebRequest()
    {
        StartCoroutine
        (
            DownloadJson
            (
                CallbackSuccess,
                CallbackFailed
            )
        );
    }

    private void CallbackSuccess(string response)
    {
        rankingList = RankingDataModel.DeserializeFromJson(response);

        _informationField.text = "Success!";
    }

    private void CallbackFailed()
    {
        _informationField.text = "WebRequest Failed";
    }

    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/gamerankingsystem/gameranking/getmessages");

        yield return www.SendWebRequest();

        if (www.error != null)
        {
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }

        else if (www.isDone)
        {
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }
}
