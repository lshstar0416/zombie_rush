﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Damage
{
    void OnDamage(float damage, Vector3 hitPoint, Vector3 hitRotation);
}
