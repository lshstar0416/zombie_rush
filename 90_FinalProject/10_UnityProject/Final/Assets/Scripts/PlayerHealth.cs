﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : Life
{
    public Slider healthSlider;

    public AudioClip deathAudio;
    public AudioClip hitAudio;
    public AudioClip pickupAudio;

    private AudioSource playerAudioPlayer;
    private Animator playerAnimator;

    private PlayerMovement playerMovement;
    private PlayerShot playerShot;

    private void Awake()
    {
        playerAnimator = GetComponent<Animator>();
        playerAudioPlayer = GetComponent<AudioSource>();

        playerMovement = GetComponent<PlayerMovement>();
        playerShot = GetComponent<PlayerShot>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        healthSlider.gameObject.SetActive(true);
        healthSlider.maxValue = basicHealth;
        healthSlider.value = health;

        playerMovement.enabled = true;
        playerShot.enabled = true;
    }

    public override void RestoreHealth(float recoveryHealth)
    {
        base.RestoreHealth(recoveryHealth);

        healthSlider.value = health;
    }

    public override void OnDamage(float damage, Vector3 hitPoint, Vector3 hitRotation)
    {
        if (!isDead)
            playerAudioPlayer.PlayOneShot(hitAudio);

        base.OnDamage(damage, hitPoint, hitRotation);

        healthSlider.value = health;
    }

    public override void Die()
    {
        base.Die();

        healthSlider.gameObject.SetActive(false);

        playerAudioPlayer.PlayOneShot(deathAudio);
        playerAnimator.SetTrigger("Die");

        playerMovement.enabled = false;
        playerShot.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isDead)
        {
            IItem item = other.GetComponent<IItem>();

            if (item != null)
            {
                item.Use(gameObject);
                playerAudioPlayer.PlayOneShot(pickupAudio);
            }
        }
    }
}
