﻿using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class RankingDataModel
{
    public static List<RankingData> DeserializeFromJson(string sStrJson)
    {
        var ret = new List<RankingData>();

        IList jsonList = (IList)Json.Deserialize(sStrJson);

        foreach (IDictionary jsonOne in jsonList)
        {
            var tmp = new RankingData();

            if (jsonOne.Contains("Name"))
            {
                tmp.Name = (string)jsonOne["Name"];
            }

            if (jsonOne.Contains("Score"))
            {
                tmp.Score = (string)jsonOne["Score"];
            }

            if (jsonOne.Contains("Date"))
            {
                tmp.Date = (string)jsonOne["Date"];
            }

            ret.Add(tmp);
        }

        return ret;
    }
}
