﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Life : MonoBehaviour, Damage
{
    public float basicHealth = 100f;
    public float health { get; protected set; }
    public bool isDead { get; protected set; }
    public event Action onDeath;

    protected virtual void OnEnable()
    {
        isDead = false;
        health = basicHealth;
    }

    public virtual void OnDamage(float damage, Vector3 hitPoint, Vector3 hitRotation)
    {
        health -= damage;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public virtual void RestoreHealth(float recoveryHealth)
    {
        if (isDead)
        {
            return;
        }

        health += recoveryHealth;
    }

    public virtual void Die()
    {
        if (onDeath != null)
        {
            onDeath();
        }

        isDead = true;
    }

}
