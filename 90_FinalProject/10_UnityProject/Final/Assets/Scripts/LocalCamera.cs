﻿using UnityEngine;
using Photon.Pun;
using Cinemachine;

public class LocalCamera : MonoBehaviourPun
{
    void Start()
    {
     if (photonView.IsMine)
        {
            CinemachineVirtualCamera followCam = FindObjectOfType<CinemachineVirtualCamera>();
            followCam.Follow = transform;
            followCam.LookAt = transform;
        }
    }
}
