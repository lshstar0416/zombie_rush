﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Monster : Life
{
    public float damage = 20f;
    public float attackDelay = 0.6f;
    private float lastAttack;

    public LayerMask targetLayer;
    private Life targetObject;
    private NavMeshAgent pathFinder;

    public ParticleSystem hitEffect;
    public AudioClip hitSound;
    public AudioClip deathSound;

    private Animator monsterAnimator;
    private AudioSource monsterAudioPlayer;
    private Renderer monsterRenderer;


    private void Awake()
    {
        pathFinder = GetComponent<NavMeshAgent>();
        monsterAnimator = GetComponent<Animator>();
        monsterAudioPlayer = GetComponent<AudioSource>();
        monsterRenderer = GetComponentInChildren<Renderer>();
    }

    private void Start()
    {
        StartCoroutine(PathUpdate());
    }

    private void Update()
    {
        monsterAnimator.SetBool("IsTarget", isTarget);
    }

    public void Setup(float monHealth, float monDamage, float monSpeed, Color monColor)
    {
        basicHealth = monHealth;
        health = monHealth;
        damage = monDamage;
        pathFinder.speed = monSpeed;
        monsterRenderer.material.color = monColor;
    }


    private bool isTarget
    {
        get
        {
            if (targetObject != null && !targetObject.isDead)
            {
                return true;
            }

            return false;
        }
    }

    private IEnumerator PathUpdate()
    {
        while (!isDead)
        {
            if (isTarget)
            {
                pathFinder.isStopped = false;
                pathFinder.SetDestination(targetObject.transform.position);
            }

            else
            {
                pathFinder.isStopped = true;

                Collider[] colliders = Physics.OverlapSphere(transform.position, 500f, targetLayer);

                for (int i = 0; i < colliders.Length; i++)
                {
                    Life life = colliders[i].GetComponent<Life>();

                    if (life != null && !life.isDead)
                    {
                        targetObject = life;
                        break;
                    }
                }
            }

            yield return new WaitForSeconds(0.3f);
        }
    }

    public override void OnDamage(float damage, Vector3 hitPoint, Vector3 hitRotation)
    {
        if (!isDead)
        {
            hitEffect.transform.position = hitPoint;
            hitEffect.transform.rotation = Quaternion.LookRotation(hitRotation);
            hitEffect.Play();
            monsterAudioPlayer.PlayOneShot(hitSound);
        }

        base.OnDamage(damage, hitPoint, hitRotation);
    }

    public override void Die()
    {
        base.Die();

        Collider[] monsterColliders = GetComponents<Collider>();

        for (int i = 0; i < monsterColliders.Length; i++)
        {
            monsterColliders[i].enabled = false;
        }

        pathFinder.isStopped = true;
        pathFinder.enabled = false;

        monsterAnimator.SetTrigger("Die");
        monsterAudioPlayer.PlayOneShot(deathSound);
    }

    private void OnTriggerStay(Collider other)
    {
        if (!isDead && Time.time >= lastAttack + attackDelay)
        {
            Life attackTarget = other.GetComponent<Life>();

            if (attackTarget != null && attackTarget == targetObject)
            {
                lastAttack = Time.time;

                Vector3 hitPoint = other.ClosestPoint(transform.position);
                Vector3 hitRotation = transform.position - other.transform.position;

                attackTarget.OnDamage(damage, hitPoint, hitRotation);
            }
        } 
    }

}
