﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = FindObjectOfType<UIManager>();
            }
            return m_instance;
        }
    }

    private static UIManager m_instance;
    public Text scoreText;
    public Text magazineText;
    public Text waveText;
    public Text wavePopUpText;
    public GameObject gameoverUI;
    public GameObject waveUI;
    public GameObject helpUI;

    public void ScoreTextUpdate(int newScore)
    {
        scoreText.text = "Score : " + newScore;
    }

    public void MagazineTextUpdate(int magNow, int magFull)
    {
        magazineText.text = magNow + "/" + magFull;
    }

    public void WaveTextUpdate(int waves, int remainMonster)
    {
        waveText.text = "残り敵あと " + remainMonster;
        wavePopUpText.text = "Wave  " + waves;
    }

    public void ActiveGameoverUI(bool active)
    {
        gameoverUI.SetActive(active);
    }

    public void ActiveWaveUI(bool active)
    {
        waveUI.SetActive(active);
    }

    public void ActiveHelpUI(bool active)
    {
        helpUI.SetActive(active);
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
