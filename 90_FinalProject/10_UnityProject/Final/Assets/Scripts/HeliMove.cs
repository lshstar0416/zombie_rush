﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeliMove : MonoBehaviour
{
    void FixedUpdate()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * 30, Space.World);
        Invoke("Wait5Sec", 15f);
    }

    void Wait5Sec()
    {
        gameObject.SetActive(false);
        gameObject.transform.position = new Vector3(22, 40, -130);
    }
}
