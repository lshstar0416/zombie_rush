﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading;

public class SceneStart : MonoBehaviour
{
    public void StartGameScene()
    {
        SceneManager.LoadScene("Map_v1");
    }
}
