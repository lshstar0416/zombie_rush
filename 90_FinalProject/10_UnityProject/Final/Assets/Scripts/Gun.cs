﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public enum PlayerState
    {
        Ready,
        Empty,
        Reload
    }

    public PlayerState state { get; private set; }

    public Transform fireTransform;
    public ParticleSystem muzzleEffect;
    private LineRenderer bulletLine;

    private AudioSource gunAudioPlay;
    public AudioClip shotAudio;
    public AudioClip reloadAudio;

    public float damage = 20;
    private float distance = 40f;
    public float fireDelay = 0.2f;
    public float reloadTime = 3.5f;
    public int maxMagazine = 20;
    public int bulletInMagazine;
    private float lastFire;

    private void Awake()
    {
        gunAudioPlay = GetComponent<AudioSource>();
        bulletLine = GetComponent<LineRenderer>();

        bulletLine.positionCount = 2;
        bulletLine.enabled = false;

        bulletInMagazine = 20;
    }

    private void OnEnable()
    {
        state = PlayerState.Ready;
        lastFire = 0;
    }

    public void Fire()
    {
        if (state == PlayerState.Ready && Time.time >= lastFire + fireDelay)
        {
            lastFire = Time.time;
            Shot();
        }
    }

    private void Shot()
    {
        RaycastHit hit;
        Vector3 hitPosition = Vector3.zero;

        if (Physics.Raycast(fireTransform.position, fireTransform.forward, out hit, distance))
        {
            Damage target = hit.collider.GetComponent<Damage>();

            if (target != null)
            {
                target.OnDamage(damage, hit.point, hit.normal);
            }

            hitPosition = hit.point;
        }

        else
        {
            hitPosition = fireTransform.position + fireTransform.forward * distance;
        }

        StartCoroutine(BulletLine(hitPosition));

        bulletInMagazine--;

        if (bulletInMagazine <= 0)
            state = PlayerState.Empty;
    }

    public bool Reload()
    {
        if (state == PlayerState.Reload || bulletInMagazine >= maxMagazine)
            return false;

        StartCoroutine(Reloading());
        return true;
    }

    private IEnumerator BulletLine(Vector3 hitPosition)
    {
        muzzleEffect.Play();
        gunAudioPlay.PlayOneShot(shotAudio);

        bulletLine.SetPosition(0, fireTransform.position);
        bulletLine.SetPosition(1, hitPosition);

        bulletLine.enabled = true;

        yield return new WaitForSeconds(0.03f);
        bulletLine.enabled = false;
    }

    private IEnumerator Reloading()
    {
        state = PlayerState.Reload;
        gunAudioPlay.PlayOneShot(reloadAudio);

        yield return new WaitForSeconds(reloadTime);

        int bulletToFill = maxMagazine - bulletInMagazine;

        bulletInMagazine += bulletToFill;

        state = PlayerState.Ready;
    }


}
