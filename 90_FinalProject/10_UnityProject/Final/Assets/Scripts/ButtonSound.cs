﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip Start;
    private void OnMouseDown()
    {
        audioSource.PlayOneShot(Start);
    }
}
